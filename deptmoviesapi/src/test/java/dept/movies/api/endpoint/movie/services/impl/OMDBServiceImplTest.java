package dept.movies.api.endpoint.movie.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import dept.movies.api.endpoint.movie.dto.in.MovieRequest;
import dept.movies.api.endpoint.movie.dto.in.builder.MovieRequestBuilder;
import dept.movies.api.endpoint.movie.dto.output.MovieResponse;
import dept.movies.api.endpoint.movie.services.OMDBService;
import dept.movies.api.endpoint.movie.util.URLUtil;
import dept.movies.api.exception.ServerException;
import dept.movies.api.util.HttpUtil;
import dept.movies.api.util.JSLTUtil;

@RunWith(MockitoJUnitRunner.class)
public class OMDBServiceImplTest {

    @Mock
    URLUtil urlUtil;

    @Mock
    HttpUtil httpUtil;

    @Mock
    JSLTUtil jsltUtil;

    @InjectMocks
    OMDBService omdbService = new OMDBServiceImpl();

    @Test
    public void searchMoviesNullResponse() throws ServerException {

        when(urlUtil.getOMDBUrl(anyInt(), anyString(), anyString()))
                .thenReturn("http://localhost:8080/movies?keyword=word&page=1");
        when(httpUtil.doSimpleRequest(anyString())).thenReturn("{}");
        when(jsltUtil.applyTemplate(anyString(), anyString())).thenReturn(null);
        MovieRequest movieRequest = new MovieRequestBuilder().build();

        MovieResponse movieResponse = omdbService.searchMovies(movieRequest, "");

        assertNull(movieResponse);
    }

    @Test
    public void searchMoviesResponse() throws ServerException {

        when(urlUtil.getOMDBUrl(anyInt(), anyString(), anyString()))
                .thenReturn("http://localhost:8080/movies?keyword=word&page=1");
        when(httpUtil.doSimpleRequest(anyString())).thenReturn("{}");
        when(jsltUtil.applyTemplate(anyString(), anyString()))
                .thenReturn("{\"movies\":[{\"name\":\"Best Movie Ever\"}]}");
        MovieRequest movieRequest = new MovieRequestBuilder().withPage(1).withKeywords("word").build();

        MovieResponse movieResponse = omdbService.searchMovies(movieRequest, "");

        assertNotNull(movieResponse);
        assertEquals("Best Movie Ever", movieResponse.getMovies().get(0).getName());
    }

}
