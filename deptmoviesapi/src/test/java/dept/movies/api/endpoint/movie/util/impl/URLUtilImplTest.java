package dept.movies.api.endpoint.movie.util.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import dept.movies.api.endpoint.movie.util.URLUtil;

@RunWith(MockitoJUnitRunner.class)
public class URLUtilImplTest {

    @InjectMocks
    URLUtil urlUtil = new URLUtilImpl();

    @Test
    public void getOMDBUrlNullValues() {
        String url = urlUtil.getOMDBUrl(0, null, null);
        assertNull(url);
    }

    @Test
    public void getOMDBUrl() {
        String url = urlUtil.getOMDBUrl(1, "word", "12345");
        assertEquals("http://www.omdbapi.com/?s=word&plot=full&page=1&apikey=12345", url);
    }

    @Test
    public void getLocalURLNullValues() {
        String url = urlUtil.getLocalURL(1, null);
        assertNull(url);
    }

    @Test
    public void getLocalURL() {
        String url = urlUtil.getLocalURL(1, "word");
        assertEquals("http://localhost:8080/movies?keywords=word&page=1", url);
    }

}
