package dept.movies.api.endpoint.movie.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import dept.movies.api.endpoint.movie.dto.in.MovieRequest;
import dept.movies.api.endpoint.movie.dto.in.builder.MovieRequestBuilder;
import dept.movies.api.endpoint.movie.dto.output.Link;
import dept.movies.api.endpoint.movie.services.LinkService;
import dept.movies.api.endpoint.movie.util.URLUtil;

@RunWith(MockitoJUnitRunner.class)
public class LinkServiceImplTest {

    @Mock
    URLUtil urlUtil;

    @InjectMocks
    LinkService linkService = new LinkServiceImpl();

    @Test
    public void getLinkTestNotEnoughMoviesToPagination() {
        MovieRequest movieRequest = new MovieRequestBuilder().withKeywords("word").withPage(1).build();
        Link link = linkService.getLink(movieRequest, 5);
        assertNull(link.getNext());
        assertNull(link.getPrevious());
    }

    @Test
    public void getLinkTestNullValue() {
        MovieRequest movieRequest = new MovieRequestBuilder().build();
        Link link = linkService.getLink(movieRequest, 0);
        assertNull(link.getNext());
        assertNull(link.getPrevious());
    }

    @Test
    public void getLinkTestEnoughMoviesToPagination() {
        MovieRequest movieRequest = new MovieRequestBuilder().withKeywords("word").withPage(2).build();
        Mockito.when(urlUtil.getLocalURL(3, "word")).thenReturn("http://localhost:8080/movies?keywords=word&page=3");
        Mockito.when(urlUtil.getLocalURL(1, "word")).thenReturn("http://localhost:8080/movies?keywords=word&page=1");
        Link link = linkService.getLink(movieRequest, 50);
        assertEquals("http://localhost:8080/movies?keywords=word&page=3", link.getNext());
        assertEquals("http://localhost:8080/movies?keywords=word&page=1", link.getPrevious());
    }

}
