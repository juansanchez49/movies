package dept.movies.api.util.impl;

import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import dept.movies.api.exception.ServerException;
import dept.movies.api.util.HttpUtil;

@RunWith(MockitoJUnitRunner.class)
public class HttpUtilImplTest {

    @InjectMocks
    HttpUtil httpUtil = new HttpUtilImpl();

    @Test
    public void doSimpleRequest() throws ServerException {
        String response = httpUtil.doSimpleRequest(null);
        assertNull(response);
    }
}
