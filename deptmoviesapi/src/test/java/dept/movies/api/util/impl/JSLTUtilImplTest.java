package dept.movies.api.util.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

import dept.movies.api.endpoint.movie.dto.output.MovieResponse;
import dept.movies.api.endpoint.movie.services.impl.OMDBServiceImpl;
import dept.movies.api.exception.ServerException;
import dept.movies.api.util.JSLTUtil;

@RunWith(MockitoJUnitRunner.class)
public class JSLTUtilImplTest {

    @InjectMocks
    JSLTUtil jsltUtil = new JSLTUtilImpl();

    @Test
    public void applyTemplateNullValues() throws ServerException {
        String response = jsltUtil.applyTemplate(null, null);
        assertNull(response);
    }

    @Test(expected = ServerException.class)
    public void applyTemplateException() throws ServerException {
        jsltUtil.applyTemplate("", "");
    }

    @Test
    public void applyTemplate() throws ServerException {
        String response = jsltUtil.applyTemplate(
                "{\"Search\":[{\"Title\":\"The Truth About the Harry Quebert Affair\",\"Year\":\"2018\",\"imdbID\":\"tt7134194\",\"Type\":\"series\",\"Poster\":\"https:\\/\\/m.media-amazon.com\\/images\\/M\\/MV5BN2YwMTVlODUtOTJmNi00NzYxLTljMmYtZGFjMzU5ODdlYzE2XkEyXkFqcGdeQXVyNDEwMzM3MTk@._V1_SX300.jpg\"},{\"Title\":\"Harry Potter and the Chamber of Secrets\",\"Year\":\"2002\",\"imdbID\":\"tt0304140\",\"Type\":\"game\",\"Poster\":\"https:\\/\\/m.media-amazon.com\\/images\\/M\\/MV5BNTM4NzQ2NjA4NV5BMl5BanBnXkFtZTgwODAwMjE4MDE@._V1_SX300.jpg\"},{\"Title\":\"Harry Enfield and Chums\",\"Year\":\"1994\\u20131999\",\"imdbID\":\"tt0108796\",\"Type\":\"series\",\"Poster\":\"https:\\/\\/m.media-amazon.com\\/images\\/M\\/MV5BNGY4NGI4OTYtOWQyYS00MzlhLWI3OTYtMzZiNGEyNTg1YzQzXkEyXkFqcGdeQXVyODY0NzcxNw@@._V1_SX300.jpg\"},{\"Title\":\"Handsome Harry\",\"Year\":\"2009\",\"imdbID\":\"tt1318001\",\"Type\":\"movie\",\"Poster\":\"https:\\/\\/m.media-amazon.com\\/images\\/M\\/MV5BMjA2MDgzNjY5M15BMl5BanBnXkFtZTcwODI5NDQzMw@@._V1_SX300.jpg\"},{\"Title\":\"Harry Potter and the Forbidden Journey\",\"Year\":\"2010\",\"imdbID\":\"tt1756545\",\"Type\":\"movie\",\"Poster\":\"https:\\/\\/m.media-amazon.com\\/images\\/M\\/MV5BNDM0YzMyNGUtMTU1Yy00OTE2LWE5NzYtZDZhMTBmN2RkNjg3XkEyXkFqcGdeQXVyMzU5NjU1MDA@._V1_SX300.jpg\"},{\"Title\":\"The Harry Hill Movie\",\"Year\":\"2013\",\"imdbID\":\"tt3013528\",\"Type\":\"movie\",\"Poster\":\"https:\\/\\/m.media-amazon.com\\/images\\/M\\/MV5BMTY3NDU5Mjg0Ml5BMl5BanBnXkFtZTgwNTQzNTk4MDE@._V1_SX300.jpg\"},{\"Title\":\"Harry & Son\",\"Year\":\"1984\",\"imdbID\":\"tt0087386\",\"Type\":\"movie\",\"Poster\":\"https:\\/\\/m.media-amazon.com\\/images\\/M\\/MV5BNzhjNzBkNDgtMGIxOC00MGQ4LThhMjQtYWIwZWNiNGRmNDk3XkEyXkFqcGdeQXVyMTMxMTY0OTQ@._V1_SX300.jpg\"},{\"Title\":\"Harry Potter and the Order of the Phoenix\",\"Year\":\"2007\",\"imdbID\":\"tt0944836\",\"Type\":\"game\",\"Poster\":\"https:\\/\\/m.media-amazon.com\\/images\\/M\\/MV5BN2VhOGI0OTItZjVhMC00MThmLWI5YzEtYTk5ZTFhMjEzOGEzXkEyXkFqcGdeQXVyNzg5OTk2OA@@._V1_SX300.jpg\"},{\"Title\":\"Who Is Harry Nilsson (And Why Is Everybody Talkin' About Him?)\",\"Year\":\"2010\",\"imdbID\":\"tt0756727\",\"Type\":\"movie\",\"Poster\":\"https:\\/\\/m.media-amazon.com\\/images\\/M\\/MV5BMTg2NTI5NDY2Ml5BMl5BanBnXkFtZTcwODQzMDMzNA@@._V1_SX300.jpg\"},{\"Title\":\"Harry Enfield's Television Programme\",\"Year\":\"1990\\u20131992\",\"imdbID\":\"tt0098817\",\"Type\":\"series\",\"Poster\":\"https:\\/\\/m.media-amazon.com\\/images\\/M\\/MV5BNDFlNWZhNzktNWQ0OC00MTM4LWIwN2MtNDA1YjY5YjljZDA5XkEyXkFqcGdeQXVyNjc4NTExMTk@._V1_SX300.jpg\"}],\"totalResults\":\"561\",\"Response\":\"True\"}",
                OMDBServiceImpl.OMDB_TEMPLATE);
        Gson gson = new Gson();
        MovieResponse movieResponse = gson.fromJson(response, MovieResponse.class);
        assertNotNull(movieResponse);
        assertEquals("The Truth About the Harry Quebert Affair", movieResponse.getMovies().get(0).getName());
        assertEquals(561, movieResponse.getTotal());
    }

}
