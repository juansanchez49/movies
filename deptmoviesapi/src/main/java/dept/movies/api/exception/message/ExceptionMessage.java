package dept.movies.api.exception.message;

import java.util.Date;

import dept.movies.api.exception.message.builder.ExceptionMessageBuilder;

public class ExceptionMessage {

    private Date timeStamp;
    private String message;

    public ExceptionMessage(ExceptionMessageBuilder builder) {
        this.setMessage(builder.getMessage());
        this.setTimeStamp(builder.getTimeStamp());
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    private void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getMessage() {
        return message;
    }

    private void setMessage(String message) {
        this.message = message;
    }
}
