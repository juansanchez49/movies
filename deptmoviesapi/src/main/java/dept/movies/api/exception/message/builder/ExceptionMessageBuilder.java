package dept.movies.api.exception.message.builder;

import java.util.Date;

import dept.movies.api.exception.message.ExceptionMessage;

public class ExceptionMessageBuilder {

    private Date timeStamp;
    private String message;

    public Date getTimeStamp() {
        return timeStamp;
    }

    public ExceptionMessageBuilder withTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;

        return this;
    }

    public String getMessage() {
        return message;
    }

    public ExceptionMessageBuilder withMessage(String message) {
        this.message = message;

        return this;
    }

    public ExceptionMessage build() {
        return new ExceptionMessage(this);
    }
}
