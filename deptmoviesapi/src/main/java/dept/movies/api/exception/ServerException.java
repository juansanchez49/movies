package dept.movies.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class ServerException extends Exception {

    private static final long serialVersionUID = 1L;

    public ServerException(String message, Throwable e) {
        super(message, e);
    }
}
