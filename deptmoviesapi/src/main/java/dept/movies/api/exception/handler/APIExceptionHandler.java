package dept.movies.api.exception.handler;

import static dept.movies.api.util.Constant.BAD_REQUEST_ERROR;
import static dept.movies.api.util.Constant.NOT_FOUND_ERROR;
import static dept.movies.api.util.Constant.SERVER_ERROR;

import java.util.Date;

import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import dept.movies.api.exception.BadRequestException;
import dept.movies.api.exception.NotFoundException;
import dept.movies.api.exception.ServerException;
import dept.movies.api.exception.message.ExceptionMessage;
import dept.movies.api.exception.message.builder.ExceptionMessageBuilder;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class APIExceptionHandler {

    Logger logger = LoggerFactory.getLogger(APIExceptionHandler.class);

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<?> badRequestException(BadRequestException ex, WebRequest request) {
        ExceptionMessage exceptionMessage = new ExceptionMessageBuilder().withMessage(ex.getMessage())
                .withTimeStamp(new Date()).build();
        logger.error(BAD_REQUEST_ERROR + " - " + ex.getMessage() + "-" + ex.getCause());
        return new ResponseEntity<>(exceptionMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<?> badRequestException(ConstraintViolationException ex, WebRequest request) {
        ExceptionMessage exceptionMessage = new ExceptionMessageBuilder().withMessage(ex.getMessage())
                .withTimeStamp(new Date()).build();
        logger.error(BAD_REQUEST_ERROR + " - " + ex.getMessage() + "-" + ex.getCause());
        return new ResponseEntity<>(exceptionMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<?> notFoundException(NotFoundException ex, WebRequest request) {
        ExceptionMessage exceptionMessage = new ExceptionMessageBuilder().withMessage(ex.getMessage())
                .withTimeStamp(new Date()).build();
        logger.error(NOT_FOUND_ERROR + " - " + ex.getMessage() + "-" + ex.getCause());
        return new ResponseEntity<>(exceptionMessage, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ServerException.class)
    public ResponseEntity<?> serverInternalException(ServerException ex, WebRequest request) {
        ExceptionMessage exceptionMessage = new ExceptionMessageBuilder().withMessage(ex.getMessage())
                .withTimeStamp(new Date()).build();
        logger.error(SERVER_ERROR + " - " + ex.getMessage() + "-" + ex.getCause());
        return new ResponseEntity<>(exceptionMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> serverException(Exception ex, WebRequest request) {
        ExceptionMessage exceptionMessage = new ExceptionMessageBuilder().withMessage(ex.getMessage())
                .withTimeStamp(new Date()).build();
        logger.error(SERVER_ERROR + " - " + ex.getMessage() + "-" + ex.getCause());
        return new ResponseEntity<>(exceptionMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
