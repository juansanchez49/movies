package dept.movies.api.endpoint.movie.util;

public interface URLUtil {

    public String getOMDBUrl(int page, String keywords, String apikey);

    public String getLocalURL(int page, String keywords);

}
