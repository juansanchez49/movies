package dept.movies.api.endpoint.movie.controller;

import dept.movies.api.endpoint.movie.dto.output.MovieResponse;

public interface IMovieController {

    MovieResponse movies(String keywords, Integer page);
}
