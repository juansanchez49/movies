package dept.movies.api.endpoint.movie.model.builder;

import dept.movies.api.endpoint.movie.model.Movie;

public class MovieBuilder {

    private String name;
    private String year;
    private String poster;
    private String urlTrailer;

    public String getName() {
        return name;
    }

    public MovieBuilder withName(String name) {
        this.name = name;

        return this;
    }

    public String getYear() {
        return year;
    }

    public MovieBuilder withYear(String year) {
        this.year = year;

        return this;
    }

    public String getUrlTrailer() {
        return urlTrailer;
    }

    public MovieBuilder withUrlTrailer(String urlTrailer) {
        this.urlTrailer = urlTrailer;

        return this;
    }

    public String getPoster() {
        return poster;
    }

    public MovieBuilder withPoster(String poster) {
        this.poster = poster;

        return this;
    }

    public Movie build() {
        return new Movie(this);
    }

}
