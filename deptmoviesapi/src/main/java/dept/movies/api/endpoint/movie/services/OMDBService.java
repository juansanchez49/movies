package dept.movies.api.endpoint.movie.services;

import dept.movies.api.endpoint.movie.dto.in.MovieRequest;
import dept.movies.api.endpoint.movie.dto.output.MovieResponse;
import dept.movies.api.exception.ServerException;

public interface OMDBService {

    MovieResponse searchMovies(MovieRequest movieRequest, String apikey) throws ServerException;

}
