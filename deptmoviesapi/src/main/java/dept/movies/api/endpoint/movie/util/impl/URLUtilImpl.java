package dept.movies.api.endpoint.movie.util.impl;

import org.springframework.stereotype.Service;

import dept.movies.api.endpoint.movie.util.URLUtil;

@Service
public class URLUtilImpl implements URLUtil {

    private static final String OMDB_SEARCH_URL = "http://www.omdbapi.com/?s=%s&plot=full&page=%d&apikey=%s";
    private static final String LOCAL_SEARCH_URL = "http://localhost:8080/movies?keywords=%s&page=%d";

    public String getOMDBUrl(int page, String keywords, String apikey) {
        if (keywords != null && apikey != null) {
            keywords = keywords.replaceAll(" ", "%20");
            return String.format(OMDB_SEARCH_URL, keywords, page, apikey);
        }
        return null;
    }

    public String getLocalURL(int page, String keywords) {
        if (keywords != null) {
            keywords = keywords.replaceAll(" ", "%20");
            return String.format(LOCAL_SEARCH_URL, keywords, page);
        }
        return null;
    }

}
