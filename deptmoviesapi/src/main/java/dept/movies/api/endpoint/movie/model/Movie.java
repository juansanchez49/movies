package dept.movies.api.endpoint.movie.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;

import dept.movies.api.endpoint.movie.model.builder.MovieBuilder;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Movie {

    @ApiModelProperty(notes = "Movie's name", name = "name", required = true, value = "Movie's name", example = "Harry Potter and the Philosopher's Stone")
    private String name;

    @ApiModelProperty(notes = "Movie's year", name = "year", required = true, value = "Movie's year", example = "2001")
    private String year;

    @ApiModelProperty(notes = "Movie's poster", name = "poster", required = true, value = "Movie's poster", example = "https://m.media-amazon.com/images/M/poster.jpg")
    private String poster;

    @ApiModelProperty(notes = "URL of movie's trailer", name = "urlTrailer", required = false, value = "URL of movie's trailer", example = "https://www.youtube.com/watch?v=eKSB0gXl9dw")
    private String urlTrailer;

    public Movie(MovieBuilder movieBuilder) {
        this.setName(movieBuilder.getName());
        this.setYear(movieBuilder.getYear());
        this.setUrlTrailer(movieBuilder.getUrlTrailer());
        this.setPoster(movieBuilder.getPoster());
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    private void setYear(String year) {
        this.year = year;
    }

    public String getUrlTrailer() {
        return urlTrailer;
    }

    private void setUrlTrailer(String urlTrailer) {
        this.urlTrailer = urlTrailer;
    }

    public String getPoster() {
        return poster;
    }

    private void setPoster(String poster) {
        this.poster = poster;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Movie))
            return false;
        Movie movie = (Movie) o;
        return Objects.equals(getName(), movie.getName()) && Objects.equals(getYear(), movie.getYear()) && Objects
                .equals(getPoster(), movie.getPoster()) && Objects.equals(getUrlTrailer(), movie.getUrlTrailer());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getName(), getYear(), getPoster(), getUrlTrailer());
    }

}
