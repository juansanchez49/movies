package dept.movies.api.endpoint.movie.dto.output.builder;

import dept.movies.api.endpoint.movie.dto.output.Link;

public class LinkBuilder {

    private String previous;
    private String next;

    public String getPrevious() {
        return previous;
    }

    public LinkBuilder withPrevious(String previous) {
        this.previous = previous;

        return this;
    }

    public String getNext() {
        return next;
    }

    public LinkBuilder withNext(String next) {
        this.next = next;

        return this;
    }

    public Link build() {
        return new Link(this);
    }
}
