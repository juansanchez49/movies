package dept.movies.api.endpoint.movie.dto.output.builder;

import java.util.List;

import dept.movies.api.endpoint.movie.dto.output.Link;
import dept.movies.api.endpoint.movie.dto.output.MovieResponse;
import dept.movies.api.endpoint.movie.model.Movie;

public class MovieResponseBuilder {

    private Link link;
    private int total;
    private List<Movie> movies;

    public Link getLink() {
        return link;
    }

    public MovieResponseBuilder withLink(Link link) {
        this.link = link;

        return this;
    }

    public int getTotal() {
        return total;
    }

    public MovieResponseBuilder withTotal(int total) {
        this.total = total;

        return this;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public MovieResponseBuilder withMovies(List<Movie> movies) {
        this.movies = movies;

        return this;
    }

    public MovieResponse build() {
        return new MovieResponse(this);
    }
}
