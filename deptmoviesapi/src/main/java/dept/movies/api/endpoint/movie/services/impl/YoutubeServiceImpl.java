package dept.movies.api.endpoint.movie.services.impl;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;

import dept.movies.api.endpoint.movie.model.Movie;
import dept.movies.api.endpoint.movie.model.builder.MovieBuilder;
import dept.movies.api.endpoint.movie.services.YoutubeService;

@Service
public class YoutubeServiceImpl implements YoutubeService {

    private static final String YOUTUBE_URL = "https://www.youtube.com/watch?v=%s";
    private static final long NUMBER_OF_VIDEOS_RETURNED = 1;
    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    private static final JsonFactory JSON_FACTORY = new JacksonFactory();

    @Cacheable(value = "youtubeCache", key = "{#movies}")
    @Override
    public List<Movie> enrichMoviesWithTrailer(List<Movie> movies, String apikey) {
        return movies.parallelStream().map(movie -> {
            return new MovieBuilder().withName(movie.getName()).withYear(movie.getYear()).withPoster(movie.getPoster())
                    .withUrlTrailer(getTrailer(movie.getName(), apikey)).build();
        }).collect(Collectors.toList());
    }

    private String getTrailer(String movieName, String apikey) {
        YouTube youtube;
        String url = null;
        if (movieName != null && apikey != null) {
            try {
                youtube = new YouTube.Builder(HTTP_TRANSPORT, JSON_FACTORY, new HttpRequestInitializer() {

                    public void initialize(HttpRequest request) throws IOException {
                    }
                }).setApplicationName("dev").build();
                String queryTerm = movieName;
                YouTube.Search.List search = youtube.search().list("id,snippet");
                String apiKey = apikey;
                search.setKey(apiKey);
                search.setQ(queryTerm);
                search.setType("video");
                search.setFields("items(id/kind,id/videoId,snippet/title,snippet/thumbnails/default/url)");
                search.setMaxResults(NUMBER_OF_VIDEOS_RETURNED);
                SearchListResponse searchResponse = search.execute();
                List<SearchResult> searchResultList = searchResponse.getItems();
                if (searchResultList != null && !searchResultList.isEmpty()) {
                    url = String.format(YOUTUBE_URL, searchResultList.get(0).getId().getVideoId());
                }
            }
            catch (Exception e) {
                url = null;
            }
        }

        return url;
    }
}
