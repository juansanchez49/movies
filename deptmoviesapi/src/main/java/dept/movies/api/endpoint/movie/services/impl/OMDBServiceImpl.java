package dept.movies.api.endpoint.movie.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import dept.movies.api.endpoint.movie.dto.in.MovieRequest;
import dept.movies.api.endpoint.movie.dto.output.MovieResponse;
import dept.movies.api.endpoint.movie.services.OMDBService;
import dept.movies.api.endpoint.movie.util.URLUtil;
import dept.movies.api.exception.ServerException;
import dept.movies.api.util.HttpUtil;
import dept.movies.api.util.JSLTUtil;

@Service
public class OMDBServiceImpl implements OMDBService {

    public static final String OMDB_TEMPLATE = "/templates/OMDB.jslt";

    @Autowired
    HttpUtil httpUtil;

    @Autowired
    JSLTUtil jsltUtil;

    @Autowired
    URLUtil urlUtil;

    @Cacheable(value = "omdbCache", key = "{#movieRequest.keywords, #movieRequest.page}")
    @Override
    public MovieResponse searchMovies(MovieRequest movieRequest, String apikey) throws ServerException {
        if (movieRequest != null && (movieRequest.getPage() != null || movieRequest.getKeywords() != null)) {
            String url = urlUtil.getOMDBUrl(movieRequest.getPage(), movieRequest.getKeywords(), apikey);
            String jsonInfo = httpUtil.doSimpleRequest(url);
            String jsonProccesed = jsltUtil.applyTemplate(jsonInfo, OMDB_TEMPLATE);
            return getMovies(jsonProccesed);
        }
        return null;
    }

    private MovieResponse getMovies(String jsonProccesed) {
        MovieResponse movieResponse = null;
        if (jsonProccesed != null) {
            Gson gson = new Gson();
            movieResponse = gson.fromJson(jsonProccesed, MovieResponse.class);
        }
        return movieResponse;
    }
}
