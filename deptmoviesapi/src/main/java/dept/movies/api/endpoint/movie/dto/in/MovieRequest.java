package dept.movies.api.endpoint.movie.dto.in;

import dept.movies.api.endpoint.movie.dto.in.builder.MovieRequestBuilder;

public class MovieRequest {

    private String keywords;
    private Integer page;

    public MovieRequest(MovieRequestBuilder movieRequestBuilder) {
        this.setKeywords(movieRequestBuilder.getKeywords());
        this.setPage(movieRequestBuilder.getPage());
    }

    public String getKeywords() {
        return keywords;
    }

    private void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Integer getPage() {
        return page;
    }

    private void setPage(Integer page) {
        this.page = page;
    }
}
