package dept.movies.api.endpoint.movie.controller.impl;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dept.movies.api.endpoint.movie.dto.in.MovieRequest;
import dept.movies.api.endpoint.movie.dto.in.builder.MovieRequestBuilder;
import dept.movies.api.endpoint.movie.dto.output.MovieResponse;
import dept.movies.api.endpoint.movie.services.LinkService;
import dept.movies.api.endpoint.movie.services.OMDBService;
import dept.movies.api.endpoint.movie.services.YoutubeService;
import dept.movies.api.exception.ServerException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@Validated
@Api(value = "Movies Consulting API", description = "Search movies in imdb and youtube trailers related")
public class MovieControllerImpl {

    private static final String OMDB_APIKEY_PROPERTY = "omdb.apikey";
    private static final String YOUTUBE_APIKEY_PROPERTY = "youtube.apikey";

    @Autowired
    private Environment env;

    @Autowired
    private OMDBService omdbService;

    @Autowired
    private YoutubeService youtubeService;

    @Autowired
    private LinkService linkService;

    @ApiOperation(value = "View a list movies", response = MovieResponse[].class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved movie list"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Server Internal Error") })
    @RequestMapping(value = "/movies", method = RequestMethod.GET)
    MovieResponse movies(
            @RequestParam(value = "keywords", required = true) @NotBlank @Size(max = 100, message = "keywords's length has to be less than 100") String keywords,
            @RequestParam(value = "page", required = true) @Min(value = 1, message = "page index has to be more than or equal to 1") Integer page)
            throws ServerException {

        MovieRequest movieRequest = new MovieRequestBuilder().withKeywords(keywords).withPage(page).build();

        MovieResponse movies = omdbService.searchMovies(movieRequest, env.getProperty(OMDB_APIKEY_PROPERTY));
        movies.setLink(linkService.getLink(movieRequest, movies.getTotal()));
        movies.setMovies(
                youtubeService.enrichMoviesWithTrailer(movies.getMovies(), env.getProperty(YOUTUBE_APIKEY_PROPERTY)));

        return movies;
    }

}
