package dept.movies.api.endpoint.movie.dto.output;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import dept.movies.api.endpoint.movie.dto.output.builder.MovieResponseBuilder;
import dept.movies.api.endpoint.movie.model.Movie;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MovieResponse {

    @ApiModelProperty(notes = "Links for navigation", name = "link", required = true, value = "Links for navigation")
    private Link link;

    @ApiModelProperty(notes = "Total of movies for search", name = "total", required = true, value = "Total of movies for search", example = "560")
    private int total;

    @ApiModelProperty(notes = "List of movies", name = "movies", required = true, value = "List of movies")
    private List<Movie> movies;

    public MovieResponse(MovieResponseBuilder movieResponseBuilder) {
        this.setMovies(movieResponseBuilder.getMovies());
        this.setLink(movieResponseBuilder.getLink());
        this.setTotal(movieResponseBuilder.getTotal());
    }

    public Link getLink() {
        return link;
    }

    public void setLink(Link link) {
        this.link = link;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

}
