package dept.movies.api.endpoint.movie.services;

import dept.movies.api.endpoint.movie.dto.in.MovieRequest;
import dept.movies.api.endpoint.movie.dto.output.Link;

public interface LinkService {

    Link getLink(MovieRequest movieRequest, int total);

}
