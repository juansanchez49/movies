package dept.movies.api.endpoint.movie.dto.output;

import com.fasterxml.jackson.annotation.JsonInclude;

import dept.movies.api.endpoint.movie.dto.output.builder.LinkBuilder;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Link {

    @ApiModelProperty(notes = "Link for previous page", name = "previous", required = false, value = "http://localhost:8080/movies?keywords=harry&page=0")
    private String previous;

    @ApiModelProperty(notes = "Link for previous page", name = "next", required = true, value = "http://localhost:8080/movies?keywords=harry&page=2")
    private String next;

    public Link(LinkBuilder linkBuilder) {
        this.setPrevious(linkBuilder.getPrevious());
        this.setNext(linkBuilder.getNext());
    }

    public String getPrevious() {
        return previous;
    }

    private void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getNext() {
        return next;
    }

    private void setNext(String next) {
        this.next = next;
    }
}
