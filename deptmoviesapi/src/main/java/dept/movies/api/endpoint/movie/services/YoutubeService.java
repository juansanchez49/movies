package dept.movies.api.endpoint.movie.services;

import java.util.List;

import dept.movies.api.endpoint.movie.model.Movie;

public interface YoutubeService {

    List<Movie> enrichMoviesWithTrailer(List<Movie> movies, String apikey);

}
