package dept.movies.api.endpoint.movie.dto.in.builder;

import dept.movies.api.endpoint.movie.dto.in.MovieRequest;

public class MovieRequestBuilder {

    private String keywords;
    private Integer page;

    public String getKeywords() {
        return keywords;
    }

    public MovieRequestBuilder withKeywords(String keywords) {
        this.keywords = keywords;

        return this;
    }

    public Integer getPage() {
        return page;
    }

    public MovieRequestBuilder withPage(Integer page) {
        this.page = page;

        return this;
    }

    public MovieRequest build() {
        return new MovieRequest(this);
    }

}
