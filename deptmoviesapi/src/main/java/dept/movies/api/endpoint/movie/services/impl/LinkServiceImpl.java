package dept.movies.api.endpoint.movie.services.impl;

import static dept.movies.api.util.Constant.MAX_NUMBER_MOVIES_PAGE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dept.movies.api.endpoint.movie.dto.in.MovieRequest;
import dept.movies.api.endpoint.movie.dto.output.Link;
import dept.movies.api.endpoint.movie.dto.output.builder.LinkBuilder;
import dept.movies.api.endpoint.movie.services.LinkService;
import dept.movies.api.endpoint.movie.util.URLUtil;

@Service
public class LinkServiceImpl implements LinkService {

    @Autowired
    URLUtil urlUtil;

    @Override
    public Link getLink(MovieRequest movieRequest, int total) {
        return new LinkBuilder().withNext(getNextPage(movieRequest, total)).withPrevious(getPreviousPage(movieRequest))
                .build();
    }

    private String getPreviousPage(MovieRequest movieRequest) {
        String previous = null;
        if (movieRequest != null && movieRequest.getPage() != null) {
            int previousPage = movieRequest.getPage() - 1;
            if (previousPage >= 1) {
                previous = urlUtil.getLocalURL(previousPage, movieRequest.getKeywords());
            }
        }

        return previous;
    }

    private String getNextPage(MovieRequest movieRequest, int total) {
        String next = null;
        int maxPage = total / MAX_NUMBER_MOVIES_PAGE;
        if (movieRequest != null && movieRequest.getPage() != null) {
            int nextPage = movieRequest.getPage() + 1;
            if (maxPage >= nextPage) {
                next = urlUtil.getLocalURL(nextPage, movieRequest.getKeywords());
            }
        }

        return next;
    }
}
