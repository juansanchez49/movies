package dept.movies.api.util;

public class Constant {

    public static final int MAX_NUMBER_MOVIES_PAGE = 10;
    public static final String SERVER_ERROR = "Server Error";
    public static final String BAD_REQUEST_ERROR = "Bad Request Error";
    public static final String NOT_FOUND_ERROR = "Not Found Error";
}
