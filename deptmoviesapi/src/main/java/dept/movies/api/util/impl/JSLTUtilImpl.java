package dept.movies.api.util.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.schibsted.spt.data.jslt.Expression;
import com.schibsted.spt.data.jslt.Parser;

import dept.movies.api.exception.ServerException;
import dept.movies.api.util.JSLTUtil;

@Service
public class JSLTUtilImpl implements JSLTUtil {

    @Override
    public String applyTemplate(String source, String templatePath) throws ServerException {
        String response = null;
        try {
            if (source != null && templatePath != null) {
                JsonNode input = getJsonNodeInput(source);
                Expression jslt = getExpression(templatePath);
                response = getOutputProcessed(input, jslt);
            }
        }
        catch (Exception e) {
            throw new ServerException("Error Applying JSLT template to Service Response", e);
        }
        return response;
    }

    private String getOutputProcessed(JsonNode input, Expression jslt) {
        String response = null;
        JsonNode output = jslt.apply(input);
        if (null != output) {
            response = output.toString();
        }
        return response;
    }

    private Expression getExpression(String templatePath) throws IOException {
        String template = readResourceContent(templatePath);
        return Parser.compileString(template);
    }

    private JsonNode getJsonNodeInput(String source) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readTree(source);
    }

    private String readResourceContent(String resource) throws IOException {
        InputStream inputStream = JSLTUtilImpl.class.getResourceAsStream(resource);
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        StringBuilder responseStrBuilder = new StringBuilder();
        String inputStr;
        while ((inputStr = br.readLine()) != null)
            responseStrBuilder.append(inputStr);
        return responseStrBuilder.toString();
    }
}
