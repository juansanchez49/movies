package dept.movies.api.util.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import dept.movies.api.exception.ServerException;
import dept.movies.api.util.HttpUtil;

@Service
public class HttpUtilImpl implements HttpUtil {

    static Logger logger = LoggerFactory.getLogger(HttpUtilImpl.class);

    @Override
    public String doSimpleRequest(String urlRequest) throws ServerException {
        String response = null;
        HttpURLConnection con = null;
        try {
            if (urlRequest != null) {
                URL url = new URL(urlRequest);
                logger.info("Doing request - " + urlRequest);
                con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
                int status = con.getResponseCode();
                if (status == HttpStatus.OK.value()) {
                    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String inputLine;
                    StringBuilder content = new StringBuilder();
                    while ((inputLine = in.readLine()) != null) {
                        content.append(inputLine);
                    }
                    response = content.toString();
                    in.close();
                } else {
                    throw new ServerException("Error doing request to OMDB", new Exception());
                }
            }
        }
        catch (Exception e) {
            throw new ServerException("Error doing request to OMDB", e);
        }
        finally {
            if (null != con) {
                con.disconnect();
            }
        }

        return response;
    }

}
