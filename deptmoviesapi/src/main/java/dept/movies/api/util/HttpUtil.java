package dept.movies.api.util;

import dept.movies.api.exception.ServerException;

public interface HttpUtil {

    public String doSimpleRequest(String urlRequest) throws ServerException;
}
