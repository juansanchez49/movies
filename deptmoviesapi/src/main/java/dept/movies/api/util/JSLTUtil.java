package dept.movies.api.util;

import dept.movies.api.exception.ServerException;

public interface JSLTUtil {

    public String applyTemplate(String source, String templatePath) throws ServerException;
}
