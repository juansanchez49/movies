const con=new Vue({
    el: '#element',
    data:{
        info:'',
        txtInput:''
    },
    mounted() {
        axios.get("http://localhost:8080/movies?keywords=disney&page=1", {
                    headers: {
                    'Access-Control-Allow-Origin': '*'
                    }
                }).then(response => {
            this.info = response.data
        })
    },
    methods:{
        searchMovies: function(){
            if(this.txtInput != '' && this.txtInput != "Please provide a keyword") {
                axios.get("http://localhost:8080/movies?keywords="+this.txtInput+"&page=1", {
                        headers: {
                        'Access-Control-Allow-Origin': '*'
                        }
                    }).then(response => {
                this.info = response.data
                })
            }else {
                this.txtInput = "Please provide a keyword"
            }
        
        },
        getPage: function(url){
            axios.get(url, {
                    headers: {
                    'Access-Control-Allow-Origin': '*'
                    }
                })
            .then(response => {
                this.info = response.data
            })
        }
    }
})
