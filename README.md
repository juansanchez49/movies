
In order to run the applicacion please clone this repository and follow the next steps

These are the sources for populating colors external service. To run please follows.

1. You'll need **`docker`** installed and runing.
2. Go to `deptmoviesapi` directory.
3. Run command: **`docker build -t IMAGE_NAME .`**
4. Run command: **`docker images`** and confirm the image `<IMAGE_NAME>` was created.
5. Run command: **`docker run -p 8080:8080 -t IMAGE_NAME`** 
6. Run command: **`docker ps`** and confirm the service container is up and runing.
7. Go to `deptmoviesapp` directory. 
8. Open the index.html file.


If you want, you can consult the API directly using the next URL http://localhost:8080/movies?keywords=marvel&page=1

Also you can access to the documentation in the next URL http://localhost:8080/swagger-ui.html

